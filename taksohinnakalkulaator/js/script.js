function arvuta(){
    var sisseastumisTasu = 1.98;
    var hinnaValikud = document.getElementById("hinnaValikud").value;
    var kmHind;
    if(hinnaValikud=="paev") {
        kmHind = 0.39;
        document.body.style.backgroundImage = "url(css/images/taxi.jpg)";
    } else {
        kmHind = 0.48;
        document.body.style.backgroundImage = "url(https://static.pexels.com/photos/30732/pexels-photo-30732.jpg)";
    }
    var km = parseFloat(document.getElementById("km").value);
    var hind = (km * kmHind) + sisseastumisTasu;
    var kmSisend = parseInt(document.getElementById("km").value);
    if (isNaN(kmSisend) || kmSisend <= 0){
        window.alert("Vale number või tühi väli");
    }else{
        document.getElementById("vastus").innerHTML = hind + "\u20AC";
        hind = hind.toFixed(2);
    }
}

